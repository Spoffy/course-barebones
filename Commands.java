import java.util.ArrayList;

//Needs seperating out into different files, perhaps a package.
//Also needs some serious refactoring, lots of nasty duplication in here, as well as unsafe argument usage.
class Clear extends Instruction {
	public static void register(Syntax s) {
		Clear w = new Clear(s.getInterpreter());
		s.addInstruction("clear",w);
	}

	public Clear(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 1) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		interpreter.setVariable(arguments.get(0), 0);
	}
}

class Incr extends Instruction {
	public static void register(Syntax s) {
		Incr w = new Incr(s.getInterpreter());
		s.addInstruction("incr",w);
	}

	public Incr(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 1) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		interpreter.setVariable(arguments.get(0), ++currentValue);
	}
}

class Decr extends Instruction {
	public static void register(Syntax s) {
		Decr w = new Decr(s.getInterpreter());
		s.addInstruction("decr",w);
	}

	public Decr(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 1) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		interpreter.setVariable(arguments.get(0), --currentValue);
	}
}

class Copy extends Instruction {
	public static void register(Syntax s) {
		Copy w = new Copy(s.getInterpreter());
		s.addInstruction("copy",w);
	}

	public Copy(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 3) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		interpreter.setVariable(arguments.get(2), currentValue);
	}
}

class Print extends Instruction {
	public static void register(Syntax s) {
		Print w = new Print(s.getInterpreter());
		s.addInstruction("print",w);
	}

	public Print(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 1) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		System.out.println(currentValue);
	}
}

class Add extends Instruction {
	public static void register(Syntax s) {
		Add w = new Add(s.getInterpreter());
		s.addInstruction("add",w);
	}

	public Add(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 2) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		int argTwo;
		if(arguments.get(1).matches("[\\d]+")) {
		   argTwo = Integer.parseInt(arguments.get(1));
		} else {
			argTwo = interpreter.getVariable(arguments.get(1));
		}
		interpreter.setVariable(arguments.get(0), currentValue + argTwo);
	}
}

class Equals extends Instruction {
	public static void register(Syntax s) {
		Equals w = new Equals(s.getInterpreter());
		s.addInstruction("equals",w);
	}

	public Equals(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 2) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		int argTwo;
		if(arguments.get(1).matches("[\\d]+")) {
		   argTwo = Integer.parseInt(arguments.get(1));
		} else {
			argTwo = interpreter.getVariable(arguments.get(1));
		}
		interpreter.setVariable(arguments.get(0), argTwo);
	}
}

class Sub extends Instruction {
	public static void register(Syntax s) {
		Sub w = new Sub(s.getInterpreter());
		s.addInstruction("sub",w);
	}

	public Sub(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		if(arguments.size() != 2) { throw new SyntaxErrorException("Invalid number of variables.", interpreter.getLine());}
		int currentValue = interpreter.getVariable(arguments.get(0));
		int argTwo;
		if(arguments.get(1).matches("[\\d]+")) {
		   argTwo = Integer.parseInt(arguments.get(1));
		} else {
			argTwo = interpreter.getVariable(arguments.get(1));
		}
		interpreter.setVariable(arguments.get(0), currentValue - argTwo);
	}
}
