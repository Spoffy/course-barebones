import java.util.ArrayList;

//IntrToken represents the string representing a single statement of code.
class InstrToken {
	private final String instruction;
	private final ArrayList<String> arguments;

	//Add some kind of safety check to this
	public InstrToken(ArrayList<String> tokens) {
		instruction = tokens.get(0);
		tokens.remove(0);
		arguments = tokens;
	}

	public String getInstruction() {
		return instruction;
	}

	public ArrayList<String> getArguments() {
		return arguments;
	}
}
