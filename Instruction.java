import java.util.ArrayList;

//Abstract class Instruction represents an implementation of an instruction.
abstract class Instruction {
	final protected Interpreter interpreter;

	public Instruction(Interpreter i) {
		interpreter = i;
	}

	abstract public void run(ArrayList<String> arguments);
}
