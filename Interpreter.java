import java.io.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.ListIterator;

import java.util.StringTokenizer;
import java.util.regex.*;

//Interpreter contains and executes the code.
//Refactoring required. Split out some methods and tidy up code.
public class Interpreter {
	public static void main(String [] args) {
		BufferedReader codeIn = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder codeBuilder = new StringBuilder();

		try {
			while(codeIn.ready()) {
				codeBuilder.append(codeIn.readLine());
			}
		} catch(IOException e) {
			System.out.println("ERROR reading std input. Message:" + e.getMessage());
		}

		Interpreter interp = new Interpreter(codeBuilder.toString());
		interp.run();
	}

	//Raw code String
	private final String code;
	private final Syntax syntax;

	//Tokens used when sorting the syntax.
	private ArrayList<InstrToken> progTokens = new ArrayList<InstrToken>(); 
	private int currentLine = 0;
	//Only support for integer variables currently for simplicities sake.
	private HashMap<String,Integer> variables = new HashMap<String, Integer>();


	public Interpreter(String code) {
		syntax = new Syntax(this);
		this.code = code;
		tokenize();
	}

	//Optimise
	protected void tokenize() {
		//First split into statements, then into commands with arguments.
		//Linked list as it's faster to append to. 
		LinkedList<String> statements = new LinkedList<String>();
		//Matching beginning with a word character, followed by any non-semicolon, ending in a semicolon. Capture removes semicolon.
		Pattern p = Pattern.compile("([\\w][^;]*);");
		Matcher m = p.matcher(code);
		while(m.find()) {
			statements.add(m.group(1));
		}

		//Now split into arguments.
		ListIterator<String> iter = statements.listIterator(0);
		while(iter.hasNext()) {
			StringTokenizer splitter = new StringTokenizer(iter.next());
			ArrayList<String> words = new ArrayList<String>(splitter.countTokens());
			while(splitter.hasMoreTokens()) {
				words.add(splitter.nextToken());
			}
			progTokens.add(new InstrToken(words));
		}
	}

	public int run() {
		for(currentLine = 0; currentLine < progTokens.size(); setLine(getLine()+1)){
			InstrToken token = progTokens.get(getLine());
			syntax.runInstruction(token);
		}
		return 0;
	}

	public String getCode() { return code; }
	
	public int getLine() { return currentLine; }

	//This is probably really, really, really unsafe and dumb. Need to get something better.
	public void setLine(int line) {	currentLine = line; }
	
	public Integer getVariable(String id) {
		Integer value = variables.get(id);
		return (value == null)? 0:value;
	}

	public void setVariable(String id, Integer value) {
		variables.put(id, value);
	}
	
	public String getInstruction(int location) {
		if(location > progTokens.size()) return null;
		return progTokens.get(location).getInstruction();
	}

	public int getNumInstructs() {
		return progTokens.size();
	}

	public int nextInstruction(int start, String id) {
		for(int i = start; i < progTokens.size(); i++) {
			if(progTokens.get(i).getInstruction().equals(id)) return i;
		}
		//-1 on unable to find.
		return -1;
	}
}
