import java.util.HashMap;
import java.util.ArrayList;

import java.util.Stack;

//Syntax - Responsible for handling instruction types and their calls.
//Needs mild refactoring.
class Syntax {
 	private HashMap<String, Instruction> instructions = new HashMap<String, Instruction>();
	final protected Interpreter pInterpreter;

	public Syntax(Interpreter i) {
		pInterpreter = i;	
		
		Clear.register(this);
		Incr.register(this);
		Decr.register(this);
		Copy.register(this);
		Print.register(this);
		While.register(this);
		Add.register(this);
		Sub.register(this);
		Equals.register(this);
	}

	public void addInstruction(String name, Instruction instance) {
		instructions.put(name, instance);
	}

	protected Interpreter getInterpreter() {
		return pInterpreter;
	}

	//Overloaded method allows for running both text commands and tokens.
	public void runInstruction(String command, ArrayList<String> arguments) {
		Instruction instr = instructions.get(command);
		//Handle missing instruction
		instr.run(arguments);
	}

	public void runInstruction(InstrToken token) {
		Instruction instr = instructions.get(token.getInstruction());
		instr.run(token.getArguments());
	}
}


