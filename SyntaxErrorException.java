class SyntaxErrorException extends RuntimeException {
	public SyntaxErrorException(String errorMessage, int line) {
		super("Syntax Error: " + errorMessage + " Line: " + line);
	}
}
