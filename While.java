import java.util.Stack;
import java.util.ArrayList;

//Strange way of doing a while loop. A code block may be better, or an object per loop.
//REFACTORING REQUIRED - REALLY GODDAMN BADLY
class While extends Instruction {
	private Stack<Integer> startLines = new Stack<Integer>();
	private Stack<Integer> endLines = new Stack<Integer>();
	private Stack<String> variables = new Stack<String>(); 
	private Stack<Integer> endVarValue = new Stack<Integer>();
	//Inherited: Interpreter interpreter;

	public static void register(Syntax s) {
		While w = new While(s.getInterpreter());
		s.addInstruction("while",w);
		s.addInstruction("end",w);
	}

	public While(Interpreter i) {
		super(i);
	}

	public void run(ArrayList<String> arguments) {
		int currentLine = interpreter.getLine();
		if(!endLines.empty() && currentLine == endLines.peek()) {
			iterateWhile();
		} else {
			newWhile(arguments);
		}
	}

	protected void iterateWhile() {
		int loopVar = interpreter.getVariable(variables.peek());
		if(loopVar == endVarValue.peek()) {
			breakLoop();
		} else {
			interpreter.setLine(startLines.peek());
		}
		
	}

	protected void newWhile(ArrayList<String> arguments) {
		int currentLine = interpreter.getLine();
		startLines.push(currentLine);

		//Refactor badly needed. Quickly hacked together.
		int loops = 1;
		int ends = 0;
		int endLoc = -1;
		for(int l=currentLine+1; l < interpreter.getNumInstructs(); l++) {
			String instruct = interpreter.getInstruction(l);
			if(instruct.equals("while")) {
				loops++;
			} else if(instruct.equals("end")) {
				ends++;
			}

			if(loops == ends){
				endLoc = l;
				break;
			}
		}
		if(endLoc < 0) { throw new SyntaxErrorException("Unable to locate end of loop.", currentLine); }
		endLines.push(endLoc);
		if(arguments.size() < 4) { throw new SyntaxErrorException("Invalid syntax of while loop", currentLine); }
		variables.push(arguments.get(0));
		endVarValue.push(Integer.valueOf(arguments.get(2)));
	}

	protected void breakLoop() {
		interpreter.setLine(endLines.pop());
		startLines.pop();
		variables.pop();
		endVarValue.pop();
	}
}
